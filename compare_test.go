package decimal

import (
	"testing"
)

func TestEqual(tester *testing.T) {
	tester.Parallel()

	testCases := []struct {
		name string
		x    string
		y    string
		want bool
	}{
		{
			name: "TC #1",
			x:    "0",
			y:    "29.11",
			want: false,
		},
		{
			name: "TC #2",
			x:    "0",
			y:    "0.00000",
			want: true,
		},
		{
			name: "TC #3",
			x:    "-0.001",
			y:    "-0.001",
			want: true,
		},
		{
			name: "TC #4",
			x:    "-1",
			y:    "-4.44",
			want: false,
		},
		{
			name: "TC #5",
			x:    "bad",
			y:    "-4.44",
			want: false,
		},
	}

	for _, tc := range testCases {
		tc := tc

		tester.Run(tc.name, func(tester *testing.T) {
			tester.Parallel()

			got := IsEqual(tc.x, tc.y)

			if got != tc.want {
				tester.Errorf("got %v but want %v;", got, tc.want)
			}
		})
	}
}

func TestIsLess(tester *testing.T) {
	tester.Parallel()

	testCases := []struct {
		name string
		x    string
		y    string
		want bool
	}{
		{
			name: "TC #1",
			x:    "0",
			y:    "29.11",
			want: true,
		},
		{
			name: "TC #2",
			x:    "0",
			y:    "0.00000",
			want: false,
		},
		{
			name: "TC #3",
			x:    "-0.001",
			y:    "-0.001",
			want: false,
		},
		{
			name: "TC #4",
			x:    "-1",
			y:    "-4.44",
			want: false,
		},
		{
			name: "TC #5",
			x:    "bad",
			y:    "-4.44",
			want: false,
		},
	}

	for _, tc := range testCases {
		tc := tc

		tester.Run(tc.name, func(tester *testing.T) {
			tester.Parallel()

			got := IsLess(tc.x, tc.y)

			if got != tc.want {
				tester.Errorf("got %v but want %v;", got, tc.want)
			}
		})
	}
}

func TestIsGreater(tester *testing.T) {
	tester.Parallel()

	testCases := []struct {
		name string
		x    string
		y    string
		want bool
	}{
		{
			name: "TC #1",
			x:    "0",
			y:    "29.11",
			want: false,
		},
		{
			name: "TC #2",
			x:    "0",
			y:    "0.00000",
			want: false,
		},
		{
			name: "TC #3",
			x:    "-0.001",
			y:    "-0.001",
			want: false,
		},
		{
			name: "TC #4",
			x:    "-1",
			y:    "-4.44",
			want: true,
		},
		{
			name: "TC #5",
			x:    "bad",
			y:    "-4.44",
			want: false,
		},
	}

	for _, tc := range testCases {
		tc := tc

		tester.Run(tc.name, func(tester *testing.T) {
			tester.Parallel()

			got := IsGreater(tc.x, tc.y)

			if got != tc.want {
				tester.Errorf("got %v but want %v;", got, tc.want)
			}
		})
	}
}
