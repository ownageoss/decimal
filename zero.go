package decimal

import (
	"github.com/cockroachdb/apd/v3"
)

// IsZero returns true if a string can be parsed as zero.
func IsZero(input string) bool {
	xDecimal, _, err := apd.NewFromString(input)
	if err != nil {
		return false
	}

	return xDecimal.IsZero()
}
