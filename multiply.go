package decimal

import (
	"errors"

	"github.com/cockroachdb/apd/v3"
)

// Multiply returns the result of x*y rounded to precision.
func Multiply(x string, y string, precision, scale int) (string, error) {
	if precision <= 0 {
		return "", errors.New("precision must be positive")
	}

	// We need a rounding specified.
	context := apd.BaseContext.WithPrecision(uint32(precision))

	result := apd.New(0, 0)

	xDecimal, _, err := apd.NewFromString(x)
	if err != nil {
		return "", err
	}

	yDecimal, _, err := apd.NewFromString(y)
	if err != nil {
		return "", err
	}

	_, err = context.Mul(result, xDecimal, yDecimal)
	if err != nil {
		return "", err
	}

	answer, err := LimitDecimalWidth(result.String(), precision, scale)
	if err != nil {
		return "", err
	}

	return answer, nil
}
