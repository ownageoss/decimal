package decimal

import (
	"github.com/cockroachdb/apd/v3"
)

// IsPositive returns true if a string can be parsed as +ve decimal.
func IsPositive(input string) bool {
	xDecimal, _, err := apd.NewFromString(input)
	if err != nil {
		return false
	}

	return !xDecimal.Negative
}
