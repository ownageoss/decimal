# Decimal

A simple string wrapper around [Cockroach DB's APD](https://github.com/cockroachdb/apd) library.

Since both JSON and Golang don't have a native decimal type, this library accepts string inputs to perform supported operations.

## Example usage

```go
package main

import (
	"gitlab.com/ownageoss/decimal"
)

func main() {
    xDecimal := "20.11"
    
    yDecimal := "30.11"

    result, err := decimal.Add([]string{xDecimal, yDecimal})
    fmt.Printf("%v, %v", result, err)
    // Output: 50.22, <nil>
}
```

Please refer to `*_test.go` files for more examples.

## Supported operations

```
- Add
- IsEqual
- IsLess
- IsGreater
- Divide
- Multiply
- IsNegative
- Power
- Subtract
- IsValidDecimal
- LimitDecimalWidth
- IsZero
```

## Should I be using this library?

First we need to understand and agree the following:

- `float` is not the same as `decimal`.
- Both Go and JSON do not have a native decimal type.
- To prevent loss of precision, decimal data is often represented as strings in JSON.

Consider the following narrow but popular use case (example: shopping cart) - Your REST API's need accurate decimal operations performed by your Go based backend to calculate the order total.

```json
// Oversimplified for the purpose of demonstration.
{
    "order": {
        [
            {
                "price": "9.99",
                "quantity": "3.0"
            },
            {
                "price": "99.99",
                "quantity": "1.0"
            }
        ]
    }
}
```

In such a situation where decimal data in represented in JSON as strings, this library maybe convenient to use.

However, if you have a DB and you need to perform arithmetic operations on data already stored in the DB, then just use SQL to perform arithmetic.

If you have a DB, but the data is not conveniently stored for arithmetic operations, you could write SQL to perform arithmetic operations.

```go
    // Example for Postgres like DB's.
    const query = `
        SELECT ($1::numeric * $2::numeric)::text
    `
    var total string
    err = db.QueryRow(query, "9.99", "3.0").Scan(&total)
```

Note: Using SQL could have performance implications. Always prototype to understand the implications.

If you have the time and patience to learn [Cockroach DB's APD](https://github.com/cockroachdb/apd), then give that a go. You may end up with something similar to this library.

## Versioning

This project uses [Semantic Versioning 2.0.0](https://semver.org/).

## Project status

This library is used in production environments and is actively maintained.

## Supported Go versions

Go version support is aligned with the Go's [release policy](https://go.dev/doc/devel/release#policy).

## Software Bill of Materials (SBOM)

```
               _      
 ___ _ __   __| |_  __
/ __| '_ \ / _` \ \/ /
\__ \ |_) | (_| |>  < 
|___/ .__/ \__,_/_/\_\
    |_|               

 📂 SPDX Document gitlab.com/ownageoss/decimal
  │ 
  │ 📦 DESCRIBES 1 Packages
  │ 
  ├ decimal
  │  │ 🔗 33 Relationships
  │  ├ CONTAINS FILE Makefile (Makefile)
  │  ├ CONTAINS FILE README.md (README.md)
  │  ├ CONTAINS FILE .gitlab-ci.yml (.gitlab-ci.yml)
  │  ├ CONTAINS FILE .gitignore (.gitignore)
  │  ├ CONTAINS FILE add.go (add.go)
  │  ├ CONTAINS FILE add_test.go (add_test.go)
  │  ├ CONTAINS FILE compare.go (compare.go)
  │  ├ CONTAINS FILE LICENSE (LICENSE)
  │  ├ CONTAINS FILE decimal.png (decimal.png)
  │  ├ CONTAINS FILE compare_test.go (compare_test.go)
  │  ├ CONTAINS FILE divide_test.go (divide_test.go)
  │  ├ CONTAINS FILE example_test.go (example_test.go)
  │  ├ CONTAINS FILE divide.go (divide.go)
  │  ├ CONTAINS FILE go.mod (go.mod)
  │  ├ CONTAINS FILE go.sum (go.sum)
  │  ├ CONTAINS FILE multiply.go (multiply.go)
  │  ├ CONTAINS FILE decimal.spdx (decimal.spdx)
  │  ├ CONTAINS FILE negative.go (negative.go)
  │  ├ CONTAINS FILE multiply_test.go (multiply_test.go)
  │  ├ CONTAINS FILE negative_test.go (negative_test.go)
  │  ├ CONTAINS FILE positive.go (positive.go)
  │  ├ CONTAINS FILE power.go (power.go)
  │  ├ CONTAINS FILE positive_test.go (positive_test.go)
  │  ├ CONTAINS FILE subtract.go (subtract.go)
  │  ├ CONTAINS FILE power_test.go (power_test.go)
  │  ├ CONTAINS FILE subtract_test.go (subtract_test.go)
  │  ├ CONTAINS FILE valid.go (valid.go)
  │  ├ CONTAINS FILE width.go (width.go)
  │  ├ CONTAINS FILE valid_test.go (valid_test.go)
  │  ├ CONTAINS FILE zero.go (zero.go)
  │  ├ CONTAINS FILE width_test.go (width_test.go)
  │  ├ CONTAINS FILE zero_test.go (zero_test.go)
  │  └ DEPENDS_ON PACKAGE github.com/cockroachdb/apd/v3@v3.2.1
  │ 
  └ 📄 DESCRIBES 0 Files
```