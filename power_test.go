package decimal

import (
	"testing"
)

func TestPower(tester *testing.T) {
	tester.Parallel()

	testCases := []struct {
		name      string
		x         string
		y         string
		precision int
		scale     int
		want      string
	}{
		{
			name:      "TC #1",
			x:         "3",
			y:         "2",
			precision: 2,
			scale:     1,
			want:      "9.0",
		},
		{
			name:      "TC #2",
			x:         "2.2",
			y:         "3.3",
			precision: 6,
			scale:     4,
			want:      "13.4895",
		},
		{
			name:      "TC #3",
			x:         "bad",
			y:         "bad",
			precision: 4,
			scale:     2,
			want:      "",
		},
		{
			name:      "TC #4",
			x:         "200",
			y:         "0",
			precision: 4,
			scale:     2,
			want:      "1.00",
		},
		{
			name:      "TC #5",
			x:         "1",
			y:         "44",
			precision: 4,
			scale:     2,
			want:      "1.00",
		},
	}

	for _, tc := range testCases {
		tc := tc

		tester.Run(tc.name, func(tester *testing.T) {
			tester.Parallel()

			got, _ := Power(tc.x, tc.y, tc.precision, tc.scale)

			if got != tc.want {
				tester.Errorf("got %v but want %v;", got, tc.want)
			}
		})
	}
}
