package decimal

import (
	"errors"
	"fmt"
	"math"

	"github.com/cockroachdb/apd/v3"
)

// LimitDecimalWidth replicates CRDB's (precision, scale) feature.
// This below method is taken directly from CRDB's repo and modified to handle string inputs.
// https://github.com/cockroachdb/cockroach/blob/master/pkg/sql/sem/tree/decimal.go#L58
func LimitDecimalWidth(x string, precision, scale int) (string, error) {
	xDecimal, _, err := apd.NewFromString(x)
	if err != nil {
		return "", err
	}

	if xDecimal.Form != apd.Finite || precision <= 0 {
		return x, nil
	}

	// Use +1 here because it is inverted later.
	if scale < math.MinInt32+1 || scale > math.MaxInt32 {
		return "", errors.New("out of range")
	}

	if scale > precision {
		return "", fmt.Errorf("scale (%d) must be between 0 and precision (%d)", scale, precision)
	}

	// decimalCtx is the default context for decimal operations. Any change
	// in the exponent limits must still guarantee a safe conversion to the
	// postgres binary decimal format in the wire protocol, which uses an
	// int16. See pgwire/types.go.
	decimalCtx := &apd.Context{
		Precision:   20,
		Rounding:    apd.RoundHalfUp,
		MaxExponent: 2000,
		MinExponent: -2000,
		// Don't error on invalid operation, return NaN instead.
		Traps: apd.DefaultTraps &^ apd.InvalidOperation,
	}

	// http://www.postgresql.org/docs/9.5/static/datatype-numeric.html
	// "If the scale of a value to be stored is greater than
	// the declared scale of the column, the system will round the
	// value to the specified number of fractional digits. Then,
	// if the number of digits to the left of the decimal point
	// exceeds the declared precision minus the declared scale, an
	// error is raised."

	ctx := decimalCtx.WithPrecision(uint32(precision))
	ctx.Traps = apd.InvalidOperation

	if _, err = ctx.Quantize(xDecimal, xDecimal, -int32(scale)); err != nil {
		var lt string
		switch v := precision - scale; v {
		case 0:
			lt = "1"
		default:
			lt = fmt.Sprintf("10^%d", v)
		}
		return "", fmt.Errorf(
			"value with precision %d, scale %d must round to an absolute value less than %s",
			precision,
			scale,
			lt,
		)
	}

	return xDecimal.String(), nil
}
