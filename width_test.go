package decimal

import (
	"testing"
)

func TestLimitDecimalWidth(tester *testing.T) {
	tester.Parallel()

	testCases := []struct {
		name      string
		x         string
		precision int
		scale     int
		want      string
	}{
		{
			name:      "TC #1",
			x:         "bad",
			precision: 4,
			scale:     4,
			want:      "",
		},
		{
			name:      "TC #2",
			x:         "143.955",
			precision: 4,
			scale:     4,
			want:      "",
		},
		{
			name:      "TC #3",
			x:         "143.955",
			precision: 4,
			scale:     2,
			want:      "",
		},
		{
			name:      "TC #4",
			x:         "55555555555555555555.555555555555555555559",
			precision: 40,
			scale:     20,
			want:      "55555555555555555555.55555555555555555556",
		},
	}

	for _, tc := range testCases {
		tc := tc

		tester.Run(tc.name, func(tester *testing.T) {
			tester.Parallel()

			got, _ := LimitDecimalWidth(tc.x, tc.precision, tc.scale)

			if got != tc.want {
				tester.Errorf("got %v but want %v;", got, tc.want)
			}
		})
	}
}
