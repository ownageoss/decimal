package decimal

import (
	"testing"
)

func TestDivide(tester *testing.T) {
	tester.Parallel()

	testCases := []struct {
		name      string
		x         string
		y         string
		precision int
		scale     int
		want      string
	}{
		{
			name:      "TC #1",
			x:         "3",
			y:         "2",
			precision: 4,
			scale:     2,
			want:      "1.50",
		},
		{
			name:      "TC #2",
			x:         "-200",
			y:         "3",
			precision: 4,
			scale:     2,
			want:      "-66.67",
		},
		{
			name:      "TC #3",
			x:         "200",
			y:         "3",
			precision: 4,
			scale:     1,
			want:      "66.7",
		},
		{
			name:      "TC #4",
			x:         "200",
			y:         "0",
			precision: 2,
			scale:     0,
			want:      "",
		},
		{
			name:      "TC #5",
			x:         "1",
			y:         "44",
			precision: 10,
			scale:     6,
			want:      "0.022727",
		},
		{
			name:      "TC #6",
			x:         "1",
			y:         "1",
			precision: -10,
			scale:     -6,
			want:      "",
		},
	}

	for _, tc := range testCases {
		tc := tc

		tester.Run(tc.name, func(tester *testing.T) {
			tester.Parallel()

			got, _ := Divide(tc.x, tc.y, tc.precision, tc.scale)

			if got != tc.want {
				tester.Errorf("got %v but want %v;", got, tc.want)
			}
		})
	}
}
