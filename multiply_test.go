package decimal

import (
	"testing"
)

func TestMultiply(tester *testing.T) {
	tester.Parallel()

	testCases := []struct {
		name      string
		x         string
		y         string
		precision int
		scale     int
		want      string
	}{
		{
			name:      "TC #1",
			x:         "3.33",
			y:         "3.33",
			precision: 4,
			scale:     2,
			want:      "11.09",
		},
		{
			name:      "TC #2",
			x:         "-200",
			y:         "3",
			precision: 4,
			scale:     2,
			want:      "",
		},
		{
			name:      "TC #3",
			x:         "200",
			y:         "0",
			precision: 4,
			scale:     1,
			want:      "0.0",
		},
		{
			name:      "TC #4",
			x:         "200",
			y:         "0",
			precision: 2,
			scale:     0,
			want:      "0",
		},
	}

	for _, tc := range testCases {
		tc := tc

		tester.Run(tc.name, func(tester *testing.T) {
			tester.Parallel()

			got, _ := Multiply(tc.x, tc.y, tc.precision, tc.scale)

			if got != tc.want {
				tester.Errorf("got %v but want %v;", got, tc.want)
			}
		})
	}
}
