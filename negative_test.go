package decimal

import (
	"testing"
)

func TestIsNegative(tester *testing.T) {
	tester.Parallel()

	testCases := []struct {
		name  string
		input string
		want  bool
	}{
		{
			name:  "TC #1",
			input: "bad",
			want:  false,
		},
		{
			name:  "TC #2",
			input: "-0.00",
			want:  true,
		},
		{
			name:  "TC #3",
			input: "0",
			want:  false,
		},
		{
			name:  "TC #4",
			input: "1e5",
			want:  false,
		},
		{
			name:  "TC #5",
			input: "12.34e-9",
			want:  false,
		},
		{
			name:  "TC #6",
			input: "-12.34e-9",
			want:  true,
		},
	}

	for _, tc := range testCases {
		tc := tc

		tester.Run(tc.name, func(tester *testing.T) {
			tester.Parallel()

			got := IsNegative(tc.input)

			if got != tc.want {
				tester.Errorf("got %v but want %v;", got, tc.want)
			}
		})
	}
}
