package decimal

import (
	"github.com/cockroachdb/apd/v3"
)

// IsEqual returns true if x == y.
func IsEqual(x string, y string) bool {
	// Disable rounding.
	context := apd.BaseContext.WithPrecision(0)

	result := apd.New(0, 0)

	xDecimal, _, err := apd.NewFromString(x)
	if err != nil {
		return false
	}

	yDecimal, _, err := apd.NewFromString(y)
	if err != nil {
		return false
	}

	_, err = context.Cmp(result, xDecimal, yDecimal)
	if err != nil {
		return false
	}

	return result.String() == "0"
}

// IsLess returns true if x < y.
func IsLess(x string, y string) bool {
	// Disable rounding.
	context := apd.BaseContext.WithPrecision(0)

	result := apd.New(0, 0)

	xDecimal, _, err := apd.NewFromString(x)
	if err != nil {
		return false
	}

	yDecimal, _, err := apd.NewFromString(y)
	if err != nil {
		return false
	}

	_, err = context.Cmp(result, xDecimal, yDecimal)
	if err != nil {
		return false
	}

	return result.String() == "-1"
}

// IsGreater returns true if x > y.
func IsGreater(x string, y string) bool {
	// Disable rounding.
	context := apd.BaseContext.WithPrecision(0)

	result := apd.New(0, 0)

	xDecimal, _, err := apd.NewFromString(x)
	if err != nil {
		return false
	}

	yDecimal, _, err := apd.NewFromString(y)
	if err != nil {
		return false
	}

	_, err = context.Cmp(result, xDecimal, yDecimal)
	if err != nil {
		return false
	}

	return result.String() == "1"
}
