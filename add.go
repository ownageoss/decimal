package decimal

import (
	"github.com/cockroachdb/apd/v3"
)

// Add returns the sum of all elements in the input array.
func Add(input []string) (string, error) {
	// Disable rounding.
	context := apd.BaseContext.WithPrecision(0)

	errDecimal := apd.MakeErrDecimal(context)

	total := apd.New(0, 0)

	for index := range input {
		item, _, err := apd.NewFromString(input[index])
		if err != nil {
			return "", err
		}
		errDecimal.Add(total, total, item)
	}

	if errDecimal.Err() != nil {
		return "", errDecimal.Err()
	}

	return total.String(), nil
}
