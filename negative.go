package decimal

import (
	"github.com/cockroachdb/apd/v3"
)

// IsNegative returns true if a string can be parsed as negative decimal.
func IsNegative(input string) bool {
	xDecimal, _, err := apd.NewFromString(input)
	if err != nil {
		return false
	}

	return xDecimal.Negative
}
