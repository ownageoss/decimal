package decimal

import (
	"testing"
)

func TestSubtract(tester *testing.T) {
	tester.Parallel()

	testCases := []struct {
		name string
		x    string
		y    string
		want string
	}{
		{
			name: "TC #1",
			x:    "0",
			y:    "29.111",
			want: "-29.111",
		},
		{
			name: "TC #2",
			x:    "0",
			y:    "-29.111",
			want: "29.111",
		},
		{
			name: "TC #3",
			x:    "bad",
			y:    "bad",
			want: "",
		},
		{
			name: "TC #4",
			x:    "1e5",
			y:    "12.34e-8",
			want: "99999.9999998766",
		},
	}

	for _, tc := range testCases {
		tc := tc

		tester.Run(tc.name, func(tester *testing.T) {
			tester.Parallel()

			got, _ := Subtract(tc.x, tc.y)

			if got != tc.want {
				tester.Errorf("got %v but want %v;", got, tc.want)
			}
		})
	}
}
