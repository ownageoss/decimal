package decimal_test

import (
	"fmt"

	"gitlab.com/ownageoss/decimal"
)

func Example_add() {
	xDecimal := "20.11"

	yDecimal := "30.11"

	result, err := decimal.Add([]string{xDecimal, yDecimal})
	fmt.Printf("%v, %v", result, err)
	// Output: 50.22, <nil>
}

func Example_subtract() {
	xDecimal := "0"

	yDecimal := "30.11"

	result, err := decimal.Subtract(xDecimal, yDecimal)
	fmt.Printf("%v, %v", result, err)
	// Output: -30.11, <nil>
}

func Example_isEqual() {
	xDecimal := "0"

	yDecimal := "30.11"

	isEqual := decimal.IsEqual(xDecimal, yDecimal)
	fmt.Printf("%v", isEqual)
	// Output: false
}

func Example_isLess() {
	xDecimal := "0"

	yDecimal := "30.11"

	isLess := decimal.IsLess(xDecimal, yDecimal)
	fmt.Printf("%v", isLess)
	// Output: true
}

func Example_isGreater() {
	xDecimal := "0"

	yDecimal := "30.11"

	isGreater := decimal.IsGreater(xDecimal, yDecimal)
	fmt.Printf("%v", isGreater)
	// Output: false
}

func Example_divide() {
	xDecimal := "3"

	yDecimal := "2"

	precision := 4

	scale := 2

	result, err := decimal.Divide(xDecimal, yDecimal, precision, scale)
	fmt.Printf("%v, %v", result, err)
	// Output: 1.50, <nil>
}

func Example_multiply() {
	xDecimal := "3"

	yDecimal := "2"

	precision := 4

	scale := 2

	result, err := decimal.Multiply(xDecimal, yDecimal, precision, scale)
	fmt.Printf("%v, %v", result, err)
	// Output: 6.00, <nil>
}

func Example_isNegative() {
	xDecimal := "-12.34e-9"

	isNegative := decimal.IsNegative(xDecimal)
	fmt.Printf("%v", isNegative)
	// Output: true
}

func Example_isPostive() {
	xDecimal := "12.34e-9"

	isPositive := decimal.IsPositive(xDecimal)
	fmt.Printf("%v", isPositive)
	// Output: true
}

func Example_power() {
	xDecimal := "3"

	yDecimal := "2"

	precision := 4

	scale := 2

	result, err := decimal.Power(xDecimal, yDecimal, precision, scale)
	fmt.Printf("%v, %v", result, err)
	// Output: 9.00, <nil>
}

func Example_isValidDecimal() {
	xDecimal := "bad"

	isValid := decimal.IsValidDecimal(xDecimal)
	fmt.Printf("%v", isValid)
	// Output: false
}

func Example_limitDecimalWidth() {
	xDecimal := "55555555555555555555.555555555555555555559"

	precision := 40

	scale := 20

	result, err := decimal.LimitDecimalWidth(xDecimal, precision, scale)
	fmt.Printf("%v, %v", result, err)
	// Output: 55555555555555555555.55555555555555555556, <nil>
}

func Example_isZero() {
	xDecimal := "-0.00"

	isZero := decimal.IsZero(xDecimal)
	fmt.Printf("%v", isZero)
	// Output: true
}
