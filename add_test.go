package decimal

import (
	"testing"
)

func TestAdd(tester *testing.T) {
	tester.Parallel()

	testCases := []struct {
		name  string
		input []string
		want  string
	}{
		{
			name:  "TC #1",
			input: []string{"20.11", "30.11"},
			want:  "50.22",
		},
		{
			name:  "TC #2",
			input: []string{"-1", "30.111"},
			want:  "29.111",
		},
		{
			name:  "TC #3",
			input: []string{"0", "0.0000"},
			want:  "0.0000",
		},
		{
			name:  "TC #4",
			input: []string{"-1", "-4.44", "10"},
			want:  "4.56",
		},
		{
			name:  "TC #5",
			input: []string{"bad", "-4.44"},
			want:  "",
		},
		{
			name:  "TC #6",
			input: []string{"1e5", "12.34e-8"},
			want:  "100000.0000001234",
		},
	}

	for _, tc := range testCases {
		tc := tc

		tester.Run(tc.name, func(tester *testing.T) {
			tester.Parallel()

			total, _ := Add(tc.input)

			if total != tc.want {
				tester.Errorf("got %v but want %v;", total, tc.want)
			}
		})
	}
}
