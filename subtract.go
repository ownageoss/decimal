package decimal

import (
	"github.com/cockroachdb/apd/v3"
)

// Subtract returns the result of x - y.
func Subtract(x string, y string) (string, error) {
	// Disable rounding.
	context := apd.BaseContext.WithPrecision(0)

	result := apd.New(0, 0)

	xDecimal, _, err := apd.NewFromString(x)
	if err != nil {
		return "", err
	}

	yDecimal, _, err := apd.NewFromString(y)
	if err != nil {
		return "", err
	}

	_, err = context.Sub(result, xDecimal, yDecimal)
	if err != nil {
		return "", err
	}

	return result.String(), nil
}
