package decimal

import (
	"github.com/cockroachdb/apd/v3"
)

// IsValidDecimal returns true if a string can be parsed into a decimal.
func IsValidDecimal(input string) bool {
	_, _, err := apd.NewFromString(input)
	return err == nil
}
